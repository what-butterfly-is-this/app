# What Butterfly Is This: Mobile Application

Cross-platform mobile application built with the [Expo](https://expo.dev/) framework for [React Native](https://reactnative.dev/).

## Install

Download the APK from the [releases](https://gitlab.com/what-butterfly-is-this/app/-/releases/) page and install it.

## Usage

When opening the app the user will be prompted for permission to use the camera. After giving such permission users can take a picture or upload an image and have it analyzed by the machine learning model.

When analysis is complete a results page will pop up with some information about the butterfly species. The option to go back allows users to repeat this process.

## Development

### Requirements

- nodeJS
- yarn (or npm) package manager
- Expo GO app on android

### Setup

run the following command to install project dependencies

```
yarn
```

### Run locally

to test the app connect a phone with USB debugging enabled and run

```
yarn android
```