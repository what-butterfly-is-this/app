import React, { createContext, useContext, useState } from 'react';

interface ButterflyInfoInterface {
  name: string;
  image_url: string;
  wikipedia_url: string;
  description: string;
  confidence: number;
}

interface ImageContextInterface {
  uri: string | null;
  setUri: (uri: string | null) => void;
  info: ButterflyInfoInterface | null;
  setInfo: (info: ButterflyInfoInterface | null) => void;
}

const ImageContext = createContext<ImageContextInterface>({
  uri: null,
  setUri: () => {},
  info: null,
  setInfo: () => {},
});

export const useImage = () => useContext(ImageContext);

export const ImageProvider = ({ children }: { children: React.ReactNode }) => {
  const [uri, setUri] = useState<string | null>(null);
  const [info, setInfo] = useState<ButterflyInfoInterface | null>(null);

  return (
    <ImageContext.Provider
      value={{
        uri,
        setUri,
        info,
        setInfo,
      }}
    >
      {children}
    </ImageContext.Provider>
  );
};
