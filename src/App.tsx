import Slider from '@react-native-community/slider';
import { Camera, CameraType, FlashMode } from 'expo-camera';
import * as FileSystem from 'expo-file-system';
import * as ImagePicker from 'expo-image-picker';
import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { Button, Pressable, Text, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import LoadingPopup from './components/LoadingPopup';
import ResultPopup from './components/ResultPopup';
import SafeAreaWrapper from './components/SafeAreaWrapper';
import { ImageProvider, useImage } from './context/ImageContext';

function HomeScreen() {
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const [camera, setCamera] = useState<Camera | null>(null);
  const [cameraIsReady, setCameraIsReady] = useState<boolean>(false);
  const [cameraIsTakingPicture, setCameraIsTakingPicture] =
    useState<boolean>(false);
  const [cameraOrientation, setCameraOrientation] = useState(CameraType.back);
  function toggleCameraOrientation() {
    setCameraOrientation((current) =>
      current === CameraType.back ? CameraType.front : CameraType.back,
    );
  }
  const [cameraFlash, setCameraFlash] = useState(FlashMode.off);
  function toggleCameraFlash() {
    setCameraFlash((current) =>
      current === FlashMode.off ? FlashMode.torch : FlashMode.off,
    );
  }
  const [cameraZoom, setCameraZoom] = useState(0);
  const [popupVisible, setPopupVisible] = useState(false);
  const { setUri, setInfo } = useImage();

  if (!permission) {
    // Camera permissions are still loading
    return <View />;
  }

  if (!permission.granted) {
    // Camera permissions are not granted yet
    return (
      <View className='flex-1 flex-col justify-center items-center bg-indigo-50'>
        <Text className='text-center mb-4'>
          The app needs permission to show the camera
        </Text>
        <Pressable
          className='bg-indigo-700 px-8 py-3 rounded-lg border-indigo-900 border-2'
          onPress={requestPermission}
        >
          <Text className='text-center text-indigo-50 font-bold'>
            allow camera usage
          </Text>
        </Pressable>
      </View>
    );
  }

  async function classify(getImageURI: () => Promise<string>) {
    try {
      const uri = await getImageURI();
      setUri(uri);

      const base64 = await FileSystem.readAsStringAsync(uri, {
        encoding: FileSystem.EncodingType.Base64,
      });
      const formData = new FormData();
      formData.append('image', base64);
      const res = await fetch(
        // 'http://192.168.1.222:3000',
        'http://ec2-54-171-254-247.eu-west-1.compute.amazonaws.com:3000',
        {
          method: 'POST',
          body: formData,
        },
      );
      const data = await res.json();
      setInfo({
        name: data.label,
        image_url: data.image,
        wikipedia_url: data.wiki,
        description: data.description,
        confidence: data.confidence,
      });
      setPopupVisible(true);
    } catch (error) {
      setUri(null);
      setInfo(null);
      setPopupVisible(false);
      alert(error);
    }
    setCameraIsTakingPicture(false);
  }

  async function takePicture() {
    await classify(async () => {
      if (!camera || !cameraIsReady || cameraIsTakingPicture)
        throw 'no camera available';

      setCameraIsTakingPicture(true);
      const picture_data = await camera.takePictureAsync();
      return picture_data.uri;
    });
  }

  async function uploadPicture() {
    await classify(async () => {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        allowsMultipleSelection: false,
        quality: 1,
      });
      if (result.canceled) throw 'upload cancelled';

      setCameraIsTakingPicture(true);
      return result.assets[0].uri;
    });
  }

  return (
    <View className='flex-1 justify-center'>
      <LoadingPopup visible={cameraIsTakingPicture} />
      <ResultPopup
        visible={popupVisible}
        setVisible={(visible: boolean) => {
          setPopupVisible(visible);
          if (!visible) {
            setUri(null);
            setInfo(null);
          }
        }}
      />
      <Camera
        className='flex-1'
        type={cameraOrientation}
        flashMode={cameraFlash}
        zoom={cameraZoom}
        ratio='16:9'
        ref={(ref) => setCamera(ref)}
        onCameraReady={() => setCameraIsReady(true)}
      >
        <View className='flex-1 flex-row bg-transparent'>
          <View className='flex-1 flex-col self-end bg-black/40  px-4 py-8'>
            <Pressable
              className='items-center bg-indigo-800 rounded-lg border-indigo-950 border-2 p-2'
              onPress={takePicture}
              disabled={!cameraIsReady || cameraIsTakingPicture}
            >
              <Text className='text-indigo-50 font-bold text-xl'>classify</Text>
            </Pressable>
            <View className='flex-row items-center content-start'>
              <Text className='text-indigo-50 '>zoom</Text>
              <Slider
                style={{ height: 40, width: '100%' }}
                value={cameraZoom}
                step={0.1}
                onValueChange={(v) => setCameraZoom(v)}
                minimumTrackTintColor='white'
                maximumTrackTintColor='white'
              />
            </View>
            <View className='flex-row gap-3 items-center content-start'>
              <Pressable
                className='flex-grow items-center content-center bg-indigo-950 py-2 rounded-lg border border-indigo-800'
                onPress={() => toggleCameraOrientation()}
              >
                <Text className='text-center text-indigo-50'>flip camera</Text>
              </Pressable>
              <Pressable
                className='flex-grow items-center content-center bg-indigo-950 p-2 rounded-lg border border-indigo-800'
                onPress={() => toggleCameraFlash()}
              >
                <Text className='text-center text-indigo-50'>
                  flash {cameraFlash === FlashMode.off ? 'on' : 'off'}
                </Text>
              </Pressable>
              <Pressable
                className='flex-grow items-center content-center bg-indigo-950 p-2 rounded-lg border border-indigo-800'
                onPress={() => uploadPicture()}
              >
                <Text className='text-center text-indigo-50'>upload</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Camera>
    </View>
  );
}

export default function App() {
  return (
    <SafeAreaProvider>
      <ImageProvider>
        <StatusBar style='light' />
        <SafeAreaWrapper>
          <HomeScreen />
        </SafeAreaWrapper>
      </ImageProvider>
    </SafeAreaProvider>
  );
}
