import { A } from '@expo/html-elements';
import { Image } from 'expo-image';
import React from 'react';
import { Modal, Pressable, ScrollView, Text, View } from 'react-native';
import { useImage } from '../context/ImageContext';

interface ResultPopupProps {
  visible: boolean;
  setVisible: (_: boolean) => void;
}

export default function ResultPopup({ visible, setVisible }: ResultPopupProps) {
  const { uri, info } = useImage();

  return (
    <Modal
      animationType='none'
      transparent={false}
      visible={visible}
      onRequestClose={() => {
        setVisible(false);
      }}
    >
      <View className='flex-1 flex-col pl-4 pr-4 bg-indigo-50'>
        {uri &&
          info &&
          (info.name === 'OTHER' ? (
            <View className='flex-1'>
              <Text className='text-2xl mt-4 font-bold'>
                There was no butterfly :(
              </Text>
              <Text>confidence: {Math.round(100 * info.confidence)}%</Text>
            </View>
          ) : (
            <>
              <View className='inline-flex flex-row mt-4 mb-4'>
                <Image
                  cachePolicy='none'
                  contentFit='cover'
                  className='flex-1 aspect-square mr-2 rounded-lg'
                  source={{ uri: uri }}
                />
                <Image
                  contentFit='cover'
                  className='flex-1 aspect-square ml-2 rounded-lg'
                  source={info.image_url}
                />
              </View>
              <Text className='uppercase font-extrabold text-2xl text-center'>
                {info.name}
              </Text>
              <Text>
                prediction confidence: {Math.round(100 * info.confidence)}%
              </Text>
              <A href={info.wikipedia_url}>
                <Text className='text-indigo-600 font-bold text-lg'>
                  [wikipedia page]
                </Text>
              </A>
              <ScrollView className='my-4'>
                <Text className='text-slate-900 text-base'>
                  {info.description}
                </Text>
              </ScrollView>
            </>
          ))}
        <Pressable
          className='mb-4 py-3 rounded-lg bg-indigo-700 border-indigo-900 border-2'
          onPress={() => setVisible(false)}
        >
          <Text className='text-indigo-50 text-center font-bold'>go back</Text>
        </Pressable>
      </View>
    </Modal>
  );
}
