import { ActivityIndicator, Modal, View, Text } from 'react-native';

interface LoadingPopupProps {
  visible: boolean;
}

export default function LoadingPopup({ visible }: LoadingPopupProps) {
  return (
    <Modal
      animationType='none'
      transparent={true}
      visible={visible}
      onRequestClose={() => {}}
    >
      <View className='flex-1 items-center content-center justify-center bg-black/75'>
        <Text className='text-white text-2xl mb-4'>analyzing image...</Text>
        <ActivityIndicator size='large' color='#3730a3' />
      </View>
    </Modal>
  );
}
