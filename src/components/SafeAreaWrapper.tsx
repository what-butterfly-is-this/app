import React from 'react';
import { View, ViewProps } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export default function SafeAreaWrapper({ children }: ViewProps) {
  const insets = useSafeAreaInsets();
  return (
    <View
      style={{
        flex: 1,
        // paddingTop: insets.top,
        paddingRight: insets.right,
        paddingBottom: insets.bottom,
        paddingLeft: insets.left,
        backgroundColor: 'transparent',
      }}
    >
      <View style={{ height: insets.top, backgroundColor: '#3730a3' }} />
      {children}
    </View>
  );
}
